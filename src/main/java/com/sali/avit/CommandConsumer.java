package com.sali.avit;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

public class CommandConsumer implements Runnable {
    private final BlockingQueue<CommandItem> commandQueue;
    private final Database database;
    private final Semaphore producerSemaphore;
    private final Semaphore consumerSemaphore;

    public CommandConsumer(BlockingQueue<CommandItem> commandQueue, Database database, Semaphore producerSemaphore, Semaphore consumerSemaphore) {
        this.commandQueue = commandQueue;
        this.database = database;
        this.producerSemaphore = producerSemaphore;
        this.consumerSemaphore = consumerSemaphore;
    }

    @Override
    public void run() {
        try {
            while (true) {
                consumerSemaphore.acquire();
                CommandItem commandItem = commandQueue.poll();
                if (commandItem == null) {
                    break;
                }

                try {
                    switch (commandItem.getCommand()) {
                        case ADD:
                            User user = commandItem.getUser();
                            database.addUser(user);
                            break;
                        case PRINT_ALL:
                            System.out.println("-- Print all users: ");
                            printAllUsers();
                            System.out.println("------ end of print --------");
                            break;
                        case DELETE_ALL:
                            database.deleteAllUsers();
                            System.out.println("-- All users deleted --------");
                            break;
                    }
                } finally {
                    producerSemaphore.release();
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void printAllUsers() {
        for (User user : database.getAllUsers()) {
            System.out.println(user);
        }
    }
}
