package com.sali.avit;

public class CommandItem {
    private CommandType commandType;
    private User user;

    public CommandItem(CommandType commandType) {
        this.commandType = commandType;
    }

    public CommandItem(CommandType commandType, User user) {
        this.commandType = commandType;
        this.user = user;
    }

    public CommandType getCommand() {
        return commandType;
    }

    public User getUser() {
        return user;
    }
}
