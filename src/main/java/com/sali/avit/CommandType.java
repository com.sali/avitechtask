package com.sali.avit;

public enum CommandType {
    ADD,
    PRINT_ALL,
    DELETE_ALL;
}
