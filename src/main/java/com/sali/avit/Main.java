package com.sali.avit;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        int queueCapacity = 3;  // Low FIFO queue capacity for demonstration purpose
        BlockingQueue<CommandItem> commandQueue = new ArrayBlockingQueue<>(queueCapacity);

        Semaphore producerSemaphore = new Semaphore(queueCapacity);
        Semaphore consumerSemaphore = new Semaphore(0);

        CommandProducer commandProducer = new CommandProducer(commandQueue, producerSemaphore, consumerSemaphore);
        Database database = new Database();
        CommandConsumer commandConsumer = new CommandConsumer(commandQueue, database, producerSemaphore, consumerSemaphore);

        Thread producerThread = new Thread(commandProducer);
        Thread consumerThread = new Thread(commandConsumer);

        producerThread.start();
        consumerThread.start();

        // Demonstrate the program using the given sequence of commands
        try {
            commandProducer.addCommand(new CommandItem(CommandType.ADD, new User(1, "a1", "Robert")));
            commandProducer.addCommand(new CommandItem(CommandType.ADD, new User(2, "a2", "Martin")));
            commandProducer.addCommand(new CommandItem(CommandType.PRINT_ALL));
            commandProducer.addCommand(new CommandItem(CommandType.DELETE_ALL));
            commandProducer.addCommand(new CommandItem(CommandType.PRINT_ALL));

/*
            commandProducer.addCommand(new CommandItem(CommandType.ADD, new User(20, "x20", "John")));
            commandProducer.addCommand(new CommandItem(CommandType.ADD, new User(21, "x21", "Ringo")));
            commandProducer.addCommand(new CommandItem(CommandType.PRINT_ALL));
 */
            producerThread.join();
            consumerThread.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
