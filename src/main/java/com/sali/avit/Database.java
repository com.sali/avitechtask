package com.sali.avit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Database {
    private EntityManagerFactory emf;
    private static final Logger logger = LoggerFactory.getLogger(Database.class);

    public Database() {
        emf = Persistence.createEntityManagerFactory("myPersistenceUnit");
    }

    public void addUser(User user) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(user);
            System.out.println("Added user: " + user.toString());
            logger.info("Added user: {}", user.getUserName());
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public void deleteAllUsers() {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("DELETE FROM Suser").executeUpdate();
            logger.info("Deleted users");
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public List<User> getAllUsers() {
        EntityManager em = emf.createEntityManager();
        logger.info("Getting all users");

        try {
            return em.createQuery("SELECT u FROM Suser u", User.class).getResultList();
        } finally {
            em.close();
        }
    }
}
