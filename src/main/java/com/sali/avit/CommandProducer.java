package com.sali.avit;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

public class CommandProducer implements Runnable {
    private final BlockingQueue<CommandItem> commandQueue;
    private final Semaphore producerSemaphore;
    private final Semaphore consumerSemaphore;

    public CommandProducer(BlockingQueue<CommandItem> commandQueue, Semaphore producerSemaphore, Semaphore consumerSemaphore) {
        this.commandQueue = commandQueue;
        this.producerSemaphore = producerSemaphore;
        this.consumerSemaphore = consumerSemaphore;
    }

    public void addCommandOLD(CommandItem commandItem) throws InterruptedException {
        producerSemaphore.acquire();
        commandQueue.put(commandItem);
        consumerSemaphore.release();
    }

    public void addCommand(CommandItem commandItem) throws InterruptedException {
        producerSemaphore.acquire();
        commandQueue.put(commandItem);
        consumerSemaphore.release();
    }

    @Override
    public void run() {
        try {
//        } catch (InterruptedException e) {

        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
    }
}
