package com.sali.avit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DatabaseTest {
    private static EntityManagerFactory emf;

    @BeforeClass
    public static void setUp() {
 //       emf = Persistence.createEntityManagerFactory("myPersistenceUnit");
    }

    @AfterClass
    public static void tearDown() {
 //       emf.close();
    }

    @Test
    public void testAddAndRetrieveUser() {
        User testUser = new User(1, "test_guid", "Test User");

        Database database = new Database();
        database.addUser(testUser);

        List<User> users = database.getAllUsers();

        assertNotNull(users);
        assertEquals(1, users.size());

        User retrievedUser = users.get(0);
        assertEquals(testUser.getUserId(), retrievedUser.getUserId());
        assertEquals(testUser.getUserGuid(), retrievedUser.getUserGuid());
        assertEquals(testUser.getUserName(), retrievedUser.getUserName());
    }
}

